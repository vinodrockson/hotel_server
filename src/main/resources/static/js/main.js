var app = angular.module('project', ['ngRoute']);

app.config(function($routeProvider) {
	$routeProvider
    	.when('/rest/booking', {
  	      controller:'HotelsController',
	      templateUrl:'views/hotels/list.html'
	    })
	    .when('/', {
	      controller:'BookingsController',
	      templateUrl:'views/hotels/booking.html'
	    })
	    .otherwise({
	      redirectTo:'/'
	    });
	});