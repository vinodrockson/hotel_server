var app = angular.module('project');

app.controller('HotelsController', function($scope, $http, $route, $location) {
	$scope.hotels = [];
	$http.get('/rest/hotels').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.hotels = data;
	});
	
});

app.controller('BookingsController', function($scope, $http, $route, $location) {
	$scope.booking = [];
	$http.get('/rest/booking').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.bookings = data;
	});
	
});