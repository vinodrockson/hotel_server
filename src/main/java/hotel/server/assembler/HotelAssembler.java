package hotel.server.assembler;

import hotel.server.controllers.HotelController;
import hotel.server.models.Hotel;
import hotel.server.resources.HotelResource;
import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;


public class HotelAssembler extends ResourceAssemblerSupport<Hotel, HotelResource>{

	public HotelAssembler() {
		super(HotelController.class, HotelResource.class);
	}

	//converts to resource
	@Override
	public HotelResource toResource(Hotel hotel) {
		HotelResource res = createResourceWithId(
				hotel.getId(), hotel);
		res.setIdres(hotel.getId());
		res.setName(hotel.getName());
		res.setRoom(hotel.getRoom());
		return res;
	}

	//converts list to resource
	public List<HotelResource> toResource(List<Hotel> hotels) {
		List<HotelResource> ress = new ArrayList<>();

		for (Hotel hotel : hotels) {
			ress.add(toResource(hotel));
		}
		return ress;
	}
}
