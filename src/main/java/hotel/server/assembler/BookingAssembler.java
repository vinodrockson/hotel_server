package hotel.server.assembler;

import hotel.server.controllers.BookingController;
import hotel.server.models.Booking;
import hotel.server.resources.BookingResource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

public class BookingAssembler extends ResourceAssemblerSupport<Booking, BookingResource> {
	
	public BookingAssembler() {
		super(BookingController.class, BookingResource.class);
		}
	
	//converts to resource
	@Override
	public BookingResource toResource(Booking book) {
		BookingResource bookResource = createResourceWithId(
			book.getId(), book);

		bookResource.setHotel(book.getHotel());
		bookResource.setIdres(book.getId());
		bookResource.setClient(book.getClient());
		bookResource.setHotelid(book.getHotelid());
		bookResource.setStartDate(book.getStartDate());
		bookResource.setEndDate(book.getEndDate());
		bookResource.setPersons(book.getPersons());
		return bookResource;	
	}

	//converts list to resource
	public List<BookingResource> toResource(List<Booking> bookList) {
	List<BookingResource> bookRes = new ArrayList<>();
	
	for (Booking sup : bookList) {
		bookRes.add(toResource(sup));
	}
	
	return bookRes;
	}
}

