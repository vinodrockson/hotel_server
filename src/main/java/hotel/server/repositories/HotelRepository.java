package hotel.server.repositories;

import hotel.server.models.Hotel;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface HotelRepository extends JpaRepository<Hotel, Long> {

	@Query("select p from Hotel p where p.name = :name and p not in (select p from Hotel p, Booking o "
			+ "where p.id = o.hotel.id and (date(:date2)between o.startDate and o.endDate or "
			+ "date(:date1) between o.startDate and o.endDate))")
	List<Hotel> isRoomAvailable(@Param("name") String name,
			@Param("date1") Date date, @Param("date2") Date date2);
}

