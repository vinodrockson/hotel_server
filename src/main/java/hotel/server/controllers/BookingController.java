package hotel.server.controllers;

import hotel.server.assembler.BookingAssembler;
import hotel.server.models.Booking;
import hotel.server.models.Hotel;
import hotel.server.repositories.BookingRepository;
import hotel.server.repositories.HotelRepository;
import hotel.server.resources.BookingResource;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/booking")
public class BookingController {

	@Autowired
	BookingRepository bRepo;
	
	@Autowired
	HotelRepository hRepo;
	
	//Gets all the bookings made from the database
	@RequestMapping(method=RequestMethod.GET, value="")
	public List<BookingResource> getAllBookings(){
		
		  List<Booking> book = bRepo.findAll();
		  BookingAssembler bookAssem = new BookingAssembler();
		  List<BookingResource> res = bookAssem.toResource(book);
		  return res;
	}
	
	//Gets the bookings made from the client
	@RequestMapping(method=RequestMethod.POST, value="")
	public ResponseEntity<BookingResource> createBooking(@RequestBody BookingResource bookRes){
		
		Booking book = new Booking();

		Hotel hotel = hRepo.findOne(bookRes.getHotelid());
		book.setHotel(hotel);
		book.setClient(bookRes.getClient());
		book.setStartDate(bookRes.getStartDate());
		book.setEndDate(bookRes.getEndDate());
		book.setPersons(bookRes.getPersons());
		book = bRepo.saveAndFlush(book);	

		BookingAssembler bookingAssembler = new BookingAssembler();
		return new ResponseEntity<BookingResource>(bookingAssembler.toResource(book), HttpStatus.CREATED);
	}
}
