package hotel.server.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import hotel.server.assembler.HotelAssembler;
import hotel.server.models.Hotel;
import hotel.server.repositories.HotelRepository;
import hotel.server.resources.HotelResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/rest/hotels")
public class HotelController {
	@Autowired
	HotelRepository hrepo;
	
	//Gets the list of all hotels from the database
	@RequestMapping(method = RequestMethod.GET, value = "")
	@ResponseStatus(HttpStatus.OK)
	public List<HotelResource> getAll() {
		List<Hotel> hotels = hrepo.findAll();
		HotelAssembler assembler = new HotelAssembler();
		List<HotelResource> res = assembler.toResource(hotels);
		return res;
	}
	
	//Gets only available hotels from the database
	@RequestMapping(method = RequestMethod.GET, value = "/query")
	@ResponseStatus(HttpStatus.OK)
	public List<HotelResource> getavailableHotels(
			@RequestParam("name") String name,
			@RequestParam("startDate") String date1,
			@RequestParam("endDate") String date2) throws Exception {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date Date1 = df.parse(date1);
		Date Date2 = df.parse(date2);

		List<Hotel> hotels = hrepo.isRoomAvailable(name, Date1, Date2);
		HotelAssembler assembler = new HotelAssembler();
		List<HotelResource> res = assembler.toResource(hotels);
		return res;
	}

}
